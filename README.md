# 4TraderX Flutter Test

4TraderX is the platform for fast and easy multi-party P2P currency exchange.

Kindly take note you are expected to do the following:
* Implement the screens in the Figma link provided below
* Design model and call FAQ API and display it in the FAQ Screen
* Send a Merge Request to the 'main' branch from a branch named firstnam-lastname-4traderx-test (eg max-smith-4traderx-test)

###Resources
-[Figma Link](https://www.figma.com/file/LL8LU2QmK6YWzFjpgWIH7S/Untitled?node-id=0%3A1)
-FAQ API https://traderx-beta-service.herokuapp.com/api/v1/faqs

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/screens/faq_screen.dart';

import 'home_screen/home_screen.dart';
import 'settings_screen.dart';
import 'wallet_screen/wallet_screen.dart';

class WrapperScreen extends StatefulWidget {
  const WrapperScreen({Key? key}) : super(key: key);

  @override
  _WrapperScreenState createState() => _WrapperScreenState();
}

class _WrapperScreenState extends State<WrapperScreen> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectedIndex,
        children: [
          HomeScreen(),
          WalletScreen(),
          FAQScreen(),
          SettingsScreen(),
          SettingsScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/house.png',
              width: 20.0,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet_outlined),
            label: 'Wallet',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.chat_bubble,
              color: Colors.transparent,
            ),
            label: 'Create',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.chat_bubble,
            ),
            label: 'Chat',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
            ),
            label: 'Settings',
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        selectedItemColor: ColorConstants.tGreen,
        unselectedItemColor: ColorConstants.tDarkGrey,
        iconSize: 24,
        onTap: (value) {
          if (value == 2 || value == 3) return;
          setState(() {
            _selectedIndex = value;
          });
        },
        elevation: 5,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        padding: EdgeInsets.all(6.0),
        decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
        child: SizedBox(
          height: 60.0,
          width: 60.0,
          child: FittedBox(
            child: FloatingActionButton(
              backgroundColor: ColorConstants.tOrange,
              elevation: 0,
              child: Icon(
                Icons.add_circle_outline_sharp,
                size: 30.0,
              ),
              onPressed: () {},
            ),
          ),
        ),
      ),
    );
  }
}

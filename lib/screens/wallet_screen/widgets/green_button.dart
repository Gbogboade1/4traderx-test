import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class GreenButton extends StatelessWidget {
  final String label;
  final IconData icon;

  const GreenButton({
    Key? key,
    required this.label,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
      color: ColorConstants.tLightGreen,
      child: Row(
        children: [
          Icon(icon, color: ColorConstants.tGreen),
          SizedBox(width: 10.0),
          Text(
            label,
            style: TextStyle(color: ColorConstants.tGreen),
          ),
        ],
      ),
    );
  }
}

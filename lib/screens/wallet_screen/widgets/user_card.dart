import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

import 'card_balance_column.dart';

class UserCard extends StatelessWidget {
  const UserCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 185,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5.0),
        child: Stack(
          children: [
            Image.asset(
              'assets/images/card.png',
              fit: BoxFit.fitWidth,
              width: double.infinity,
              // height: 185,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset(
                            'assets/images/icon.png',
                            width: 25.0,
                            fit: BoxFit.fitWidth,
                          ),
                          SizedBox(width: 8.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset(
                                'assets/images/traderX.png',
                                width: 73,
                                height: 12.0,
                              ),
                              // Text(
                              //   '4TraderX',
                              //   style: TextStyle(
                              //     color: Colors.white,
                              //     fontSize: 15.0,
                              //     fontWeight: FontWeight.w800,
                              //   ),
                              // ),
                              Text(
                                'USD',
                                style: TextStyle(color: Color(0xff69DB85)),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Text(
                        '1234567891',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 12.0),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      CardBalanceColumn(
                        header: 'Available funds',
                        balance: '0.00',
                      ),
                      SizedBox(width: 50.0),
                      CardBalanceColumn(
                        header: 'Spending power',
                        balance: '0.00',
                      ),
                    ],
                  ),
                  Text(
                    '4785 1875 6745 7386',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                    ),
                  ),
                  Text(
                    'Damilola Ibitayo',
                    style: TextStyle(
                      color: ColorConstants.tLightGrey,
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                      height: 1.5,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

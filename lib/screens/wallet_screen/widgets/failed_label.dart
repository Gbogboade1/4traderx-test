import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class FailedLabel extends StatelessWidget {
  const FailedLabel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3),
      color: ColorConstants.tLightError,
      child: Center(
          child: Text(
        'Failed',
        style: TextStyle(
          color: ColorConstants.tError,
        ),
      )),
    );
  }
}

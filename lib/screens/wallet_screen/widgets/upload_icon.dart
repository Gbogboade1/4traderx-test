import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class UploadIcon extends StatelessWidget {
  const UploadIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 15.0,
      backgroundColor: ColorConstants.tLightestGrey,
      child: SizedBox(
        height: 20,
        width: 20,
        child: Image.asset(
          'assets/images/upload.png',
          fit: BoxFit.scaleDown,
        ),
      ),
    );
  }
}

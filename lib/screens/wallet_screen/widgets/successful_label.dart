import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class SuccessfulLabel extends StatelessWidget {
  const SuccessfulLabel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3),
      color: ColorConstants.tLightGreen,
      child: Center(
          child: Text(
        'Successful',
        style: TextStyle(
          color: ColorConstants.tGreen,
        ),
      )),
    );
  }
}

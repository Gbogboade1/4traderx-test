import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class CardBalanceColumn extends StatelessWidget {
  final String header;
  final String balance;

  CardBalanceColumn({
    required this.header,
    required this.balance,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          header,
          style: TextStyle(
            color: ColorConstants.tLightGrey,
            fontWeight: FontWeight.w500,
            fontSize: 12.0,
          ),
        ),
        Text(
          '\$${balance}',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16.0,
          ),
        ),
      ],
    );
  }
}

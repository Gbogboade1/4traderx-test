import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/constants/text_style_costants.dart';

class BottomHeaderRow extends StatelessWidget {
  final String title;

  const BottomHeaderRow({
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyleConstants.header2,
          ),
          Row(
            children: [
              Text(
                'View all',
                style: TextStyleConstants.viewAll,
              ),
              SizedBox(width: 5.0),
              Icon(
                Icons.arrow_forward_ios,
                size: 12,
                color: ColorConstants.tOrange,
              ),
              SizedBox(width: 16.0),
            ],
          )
        ],
      ),
    );
  }
}

import 'dart:core';

import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/screens/wallet_screen/widgets/bottom_header_row.dart';
import 'package:traderx/screens/wallet_screen/widgets/failed_label.dart';
import 'package:traderx/screens/wallet_screen/widgets/green_button.dart';
import 'package:traderx/screens/wallet_screen/widgets/successful_label.dart';
import 'package:traderx/screens/wallet_screen/widgets/upload_icon.dart';
import 'package:traderx/screens/wallet_screen/widgets/user_card.dart';
import 'package:traderx/utils/shared_widgets/custom_app_bar.dart';
import 'package:traderx/utils/shared_widgets/custom_list_tile.dart';

class WalletScreen extends StatelessWidget {
  const WalletScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        trailingIcon: false,
        widget: Row(
          children: [
            Text(
              'Wallets',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 20.0,
              ),
            ),
            SizedBox(width: 10.0),
            Icon(Icons.keyboard_arrow_down_outlined),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
        child: Column(
          children: [
            UserCard(),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GreenButton(
                  icon: Icons.cloud_upload_outlined,
                  label: 'Fund Wallet',
                ),
                GreenButton(
                  icon: Icons.cloud_download_outlined,
                  label: 'Withdraw',
                ),
              ],
            ),
            BottomHeaderRow(
              title: 'Recent Transactions',
            ),
            Expanded(
              child: DefaultTabController(
                length: 3,
                child: Scaffold(
                  body: Column(
                    children: [
                      Container(
                        height: 40.0,
                        child: TabBar(
                          indicatorColor: Color(0xff347345),
                          labelColor: Colors.black,
                          unselectedLabelColor: ColorConstants.tDarkGrey,
                          unselectedLabelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            fontWeight: FontWeight.w400,
                          ),
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500,
                          ),
                          tabs: [
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              text: 'Fund',
                            ),
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              text: 'Withdrawal',
                            ),
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              text: 'Exchange',
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            ListView.builder(
                              itemBuilder: (context, index) {
                                if (index == 1 || index == 3) {
                                  return const CustomListTile(
                                    icon: UploadIcon(),
                                    title: '234FRRTTUUG5564',
                                    subTitle: '23 Nov, 2021',
                                    trailingTitle: '\$23,000.00',
                                    trailingWidget: FailedLabel(),
                                  );
                                } else {
                                  return const CustomListTile(
                                    icon: UploadIcon(),
                                    title: '234FRRTTUUG5564',
                                    subTitle: '23 Nov, 2021',
                                    trailingTitle: '\$23,000.00',
                                    trailingWidget: SuccessfulLabel(),
                                  );
                                }
                              },
                              padding: EdgeInsets.only(top: 24),
                            ),
                            Container(
                              child: Center(child: Text('Withdrawal Tab')),
                            ),
                            Container(
                              child: Center(child: Text('Exchange Tab')),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

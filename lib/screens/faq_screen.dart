import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/models/faq.dart';
import 'package:traderx/utils/services/api_service.dart';
import 'package:traderx/utils/shared_widgets/custom_app_bar.dart';

class FAQScreen extends StatefulWidget {
  FAQScreen({Key? key}) : super(key: key);

  @override
  State<FAQScreen> createState() => _FAQScreenState();
}

class _FAQScreenState extends State<FAQScreen> {
  bool _expanded = false;

  late Future<List<Faq>> _faqList;

  @override
  void initState() {
    // TODO: implement initState
    _faqList = APIService().getFaqs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        trailingIcon: false,
        widget: Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 20.0,
                ),
              ),
              Text('FAQ'),
              Visibility(
                visible: false,
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 20.0,
                ),
              ),
            ],
          ),
        ),
      ),
      body: FutureBuilder<List<Faq>>(
        future: _faqList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  child: ExpansionPanelList.radio(
                    animationDuration: Duration(milliseconds: 500),
                    children: [
                      ExpansionPanelRadio(
                        headerBuilder: (context, isExpanded) {
                          return ListTile(
                            title: Text(
                              snapshot.data![0].question!,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          );
                        },
                        body: Padding(
                          padding: const EdgeInsets.only(
                              left: 16.0, right: 16.0, bottom: 12.0),
                          child: Text(snapshot.data![0].answer!,
                              style: TextStyle(color: Colors.black)),
                        ),
                        // isExpanded: _expanded,
                        canTapOnHeader: true, value: 2,
                      ),
                      ExpansionPanelRadio(
                        headerBuilder: (context, isExpanded) {
                          return ListTile(
                            title: Text(
                              snapshot.data![1].question!,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          );
                        },
                        body: Padding(
                          padding: const EdgeInsets.only(
                              left: 16.0, right: 16.0, bottom: 12.0),
                          child: Text(snapshot.data![1].answer!,
                              style: TextStyle(color: Colors.black)),
                        ),
                        // isExpanded: _expanded,
                        canTapOnHeader: true, value: 1,
                      ),
                    ],
                    dividerColor: Colors.grey,
                    expansionCallback: (panelIndex, isExpanded) {
                      _expanded = !_expanded;
                      setState(() {});
                    },
                  ),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return const Center(
              child: Text('An Error occurred\n Please try again'),
            );
          }
          return Center(
              child: CircularProgressIndicator(
            color: ColorConstants.tOrange,
            strokeWidth: 3,
          ));
        },
      ),
    );
  }
}

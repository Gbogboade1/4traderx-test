import 'package:flutter/material.dart';
import 'package:traderx/constants/text_style_costants.dart';
import 'package:traderx/screens/faq_screen.dart';
import 'package:traderx/utils/shared_widgets/custom_app_bar.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        trailingIcon: false,
        widget: Text(
          'Settings',
          style: TextStyleConstants.appBarHeader,
        ),
      ),
      body: Column(
        children: [
          SettingTile(
            leading: Image.asset(
              'assets/images/shield.png',
              width: 20.0,
              height: 20.0,
              fit: BoxFit.fitWidth,
            ),
            onTap: () {},
          ),
          SettingTile(
            title: 'FAQ',
            leading: Image.asset(
              'assets/images/question_circle.png',
              width: 20.0,
              height: 20.0,
              fit: BoxFit.fitWidth,
            ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => FAQScreen(),
              ));
            },
          ),
          SettingTile(
            title: 'Invite Friends',
            leading: Image.asset(
              'assets/images/invite_friends.png',
              width: 20.0,
              height: 20.0,
              fit: BoxFit.fitWidth,
            ),
            onTap: () {},
          ),
          SettingTile(
            title: 'Link Account',
            leading: Icon(Icons.share, size: 20.0),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}

class SettingTile extends StatelessWidget {
  final String title;
  final Widget leading;
  final void Function() onTap;

  const SettingTile({
    Key? key,
    this.title = 'Security',
    required this.leading,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: leading,
          title: Text(title),
          trailing: Icon(
            Icons.arrow_forward_ios_rounded,
            size: 15,
          ),
          onTap: onTap,
        ),
        Divider(),
      ],
    );
  }
}

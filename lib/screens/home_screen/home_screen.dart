import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/constants/text_style_costants.dart';
import 'package:traderx/screens/home_screen/widgets/needs_card.dart';
import 'package:traderx/screens/home_screen/widgets/partly_fulfilled_label.dart';
import 'package:traderx/screens/wallet_screen/widgets/bottom_header_row.dart';
import 'package:traderx/utils/shared_widgets/custom_app_bar.dart';
import 'package:traderx/utils/shared_widgets/custom_list_tile.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  List askList = ['30,000', '40,000'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        widget: Row(
          children: [
            CircleAvatar(
              radius: 12.0,
              child: Image.asset('assets/images/face.png'),
            ),
            SizedBox(width: 8.0),
            Text(
              'Olamide',
              style: TextStyleConstants.appBarHeader,
            ),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 27.0, left: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'My Asks',
              style: TextStyleConstants.header2,
            ),
            SizedBox(
              height: 16.0,
            ),
            Container(
              height: 77,
              margin: EdgeInsets.only(bottom: 10.0, top: 0.0),
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: askList.length,
                itemBuilder: (context, index) {
                  return NeedsCard(
                    askList: askList,
                    index: index,
                  );
                },
              ),
            ),
            BottomHeaderRow(title: 'All My Asks'),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 24.0),
              child: Row(
                children: [
                  Column(
                    children: [
                      Text(
                        'Other Asks',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(height: 4.0),
                      Container(
                        height: 1,
                        width: 75.0,
                        color: Colors.black,
                      )
                    ],
                  ),
                  SizedBox(width: 23.0),
                  Column(
                    children: [
                      Text(
                        'My Bids',
                        style: TextStyle(
                          color: ColorConstants.tDarkGrey,
                          fontWeight: FontWeight.w500,
                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(height: 4.0),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.only(right: 16.0),
                  itemBuilder: (context, index) {
                    if (index == 1 || index == 3) {
                      return const CustomListTile(
                        trailingTitle: 'Have \u20A62300',
                        trailingWidget: PartlyFulfilledLabel(),
                      );
                    } else {
                      return CustomListTile(
                        trailingTitle: 'Have\u20A62300',
                      );
                    }
                  }),
            )
          ],
        ),
      ),
      backgroundColor: ColorConstants.tbackgroundColor,
    );
  }
}

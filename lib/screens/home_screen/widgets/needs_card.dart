import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/utils/shared_widgets/open_label.dart';

import 'hand_icon.dart';

class NeedsCard extends StatelessWidget {
  final int? index;
  final List? askList;
  const NeedsCard({
    Key? key,
    this.index,
    this.askList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 16.0),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 14),
      // height: 77.0,
      width: MediaQuery.of(context).size.width * 0.75,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1.0,
          color: ColorConstants.tLightestGrey,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              HandIcon(),
              SizedBox(width: 8.0),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Need \u20A6${askList![index!]}',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                    ),
                  ),
                  Text('@540'),
                ],
              ),
            ],
          ),
          OpenLabel(),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class PartlyFulfilledLabel extends StatelessWidget {
  const PartlyFulfilledLabel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3),
      color: Color(0xffFFF4DB),
      child: Center(
          child: Text(
        'Partly Fulfilled',
        style: TextStyle(
          color: Color(0xffD6981B),
        ),
      )),
    );
  }
}

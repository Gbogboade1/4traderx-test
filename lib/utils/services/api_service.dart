import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:traderx/models/faq.dart';

class APIService {
  Future<List<Faq>> getFaqs() async {
    List<Faq> faqList = [];
    try {
      var response = await http
          .get(
            Uri.parse('https://traderx-beta-service.herokuapp.com/api/v1/faqs'),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        // log(response.body);
        var responseData = jsonDecode(response.body) as List;
        print(responseData);
        responseData.forEach((element) {
          faqList.add(Faq.fromJson(element));
        });
        print(faqList);
        var data = Faq.fromJson(responseData[0]);
        print(data.answer!);
        return faqList;
      }
      throw Exception();
    } catch (e) {
      throw Error();
    }
  }
}

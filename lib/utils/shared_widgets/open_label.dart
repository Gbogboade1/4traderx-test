import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class OpenLabel extends StatelessWidget {
  const OpenLabel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 20,
      margin: EdgeInsets.all(2),
      color: ColorConstants.tLightBlue,
      child: Center(
          child: Text(
        'Open',
        style: TextStyle(color: ColorConstants.tBlue),
      )),
    );
  }
}

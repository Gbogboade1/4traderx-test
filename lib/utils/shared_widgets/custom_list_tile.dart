import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';
import 'package:traderx/screens/home_screen/widgets/hand_icon.dart';

import 'open_label.dart';

class CustomListTile extends StatelessWidget {
  final Widget icon;
  final String title;
  final String subTitle;
  final String trailingTitle;
  final Widget trailingWidget;

  const CustomListTile({
    Key? key,
    this.icon = const HandIcon(),
    this.title = 'Need \$106',
    this.subTitle = '@400',
    this.trailingTitle = '',
    this.trailingWidget = const OpenLabel(),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                icon,
                SizedBox(width: 16.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      subTitle,
                      style: TextStyle(
                        color: ColorConstants.tDarkGrey,
                        fontWeight: FontWeight.w400,
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  trailingTitle,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0,
                  ),
                ),
                SizedBox(height: 10.0),
                trailingWidget,
              ],
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Divider(),
        ),
      ],
    );
  }
}

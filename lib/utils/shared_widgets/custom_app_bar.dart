import 'package:flutter/material.dart';
import 'package:traderx/constants/color_constants.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget widget;
  final bool trailingIcon;
  const CustomAppBar({
    Key? key,
    this.trailingIcon = true,
    required this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: ColorConstants.tLightGrey),
        ),
      ),
      padding: EdgeInsets.only(bottom: 15.0, left: 16.0, right: 16.0),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            widget,
            trailingIcon
                ? GestureDetector(
                    onTap: () {},
                    child: Icon(Icons.notifications_none),
                  )
                : SizedBox.shrink(),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(75);
}

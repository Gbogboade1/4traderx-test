/// id : 5
/// createdOn : "2022-03-29T10:02:07.000+00:00"
/// lastModifiedOn : "2022-03-29T10:02:07.000+00:00"
/// question : "How does 4Traderx work?"
/// answer : "4Traderx is a peer-to-peer currency exchange platform that allows you to trade your local currency for another instantly. You will specify how much of your local money you have and at what rate you are willing to exchange it. You can share your Ask link with someone to match it or wait for someone to bid on it."

class Faq {
  Faq({
    this.id,
    this.createdOn,
    this.lastModifiedOn,
    this.question,
    this.answer,
  });

  Faq.fromJson(dynamic json) {
    id = json['id'];
    createdOn = json['createdOn'];
    lastModifiedOn = json['lastModifiedOn'];
    question = json['question'];
    answer = json['answer'];
  }
  int? id;
  String? createdOn;
  String? lastModifiedOn;
  String? question;
  String? answer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdOn'] = createdOn;
    map['lastModifiedOn'] = lastModifiedOn;
    map['question'] = question;
    map['answer'] = answer;
    return map;
  }
}

import 'package:flutter/material.dart';

import 'color_constants.dart';

class TextStyleConstants {
  static final appBarHeader = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  );
  static final header2 = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
  );

  static final viewAll = TextStyle(
    color: ColorConstants.tOrange,
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
  );
}

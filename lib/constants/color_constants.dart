import 'dart:ui';

class ColorConstants {
  static final Color tOrange = Color(0xffEC7E3D);
  static final Color tLightGrey = Color(0xffE1E1E3);
  static final Color tLightestGrey = Color(0xffF5F5F5);
  static final Color tDarkGrey = Color(0xff5A5B6A);
  static final Color tGreen = Color(0xff00A859);
  static final Color tLightBlue = Color(0xffE7F0FF);
  static final Color tBlue = Color(0xff4285F4);
  static final Color tbackgroundColor = Color(0xffffffff);
  static final Color tLightGreen = Color(0xffE0FFE8);
  static final Color tError = Color(0xffF44336);
  static final Color tLightError = Color(0xffFFEDEC);
}
